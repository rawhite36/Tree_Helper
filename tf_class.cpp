//Created by Ryan Whitehead, 07-10-16
//tf member definitions


#include <iostream>
#include <iomanip>
#include <cstdio>
#include <fstream>
#include <cctype>
#include <vector>
#include <string>
#include <cstring>
#include <algorithm>
#include <sys/sysinfo.h>
#include <TTree.h>
#include <TH1.h>
#include <TGraph.h>
#include <TFile.h>
#include <TKey.h>
#include <TMath.h>
#include <TROOT.h>
#include "inc/tf_class.hpp"
#include "inc/tf_gui.hpp"

using namespace std;


//Set counter for draw function
UChar_t tf::nn = 1;
TTree* tf::t_ = NULL;


tf::tf()
{
  //initialize data members
	histemp[0] = NULL; histemp[1] = NULL;
	gratemp = NULL;
	n = 1;
  nn = 1;
  t_ = NULL;
	for(UChar_t i=0; i<11; ++i)
		flags[i] = 0;
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


tf::tf(string fname, string pnt)
{
	//initialize data members
	histemp[0] = NULL; histemp[1] = NULL;
	gratemp = NULL;
	f = fname;
	n = 1;
  nn = 1;
  t_ = NULL;
	for(UChar_t i=0; i<11; ++i)
		flags[i] = 0;

	//check to see if the file is open, sort file, add file to list with
	//referencing pointer, create tree, perform merge-sort, fill tree,
	//and assign referencing pointer to tree in interpreter's scope
	string filetemp = f.substr(f.find_last_of('.')+1,-1);
	for(UShort_t i=0; i<filetemp.length(); ++i)
		tolower(filetemp[i]);

	if(filetemp!="root")
	{
		if(getb(pnt)==1)
			cout << "\n  Cannot Open File\n\n";
	}
	else
	{
		UChar_t check = 0;
		if((check = getr(pnt))==1)
			cout << "\n  Cannot Open File\n\n";

		if(check==2)
			cout << "\n  There Are No Trees In File\n\n";
	}
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


tf::tf(string fname, string pnt, ULong64_t head, string data)
{
	//initialize data members
	histemp[0] = NULL; histemp[1] = NULL;
	gratemp = NULL;
	f = fname;
	n = 1;
  nn = 1;
  t_ = NULL;
	for(UChar_t i=0; i<11; ++i)
		flags[i] = 0;

	//check to see if the file is open, sort file, add file to list with
	//referencing pointer, create tree, perform merge-sort, fill tree,
	//and assign referencing pointer to tree in interpreter's scope
	string filetemp = f.substr(f.find_last_of('.')+1,-1);
	for(UShort_t i=0; i<filetemp.length(); ++i)
		tolower(filetemp[i]);

	if(filetemp!="root")
	{
		if(getb(pnt,head,data)==1)
			cout << "\n  Cannot Open File\n\n";
	}
	else
	{
		UChar_t check = 0;
		if((check = getr(pnt))==1)
			cout << "\n  Cannot Open File\n\n";

		if(check==2)
			cout << "\n  There Are No Trees In File\n\n";
	}
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


tf::~tf()
{
  for(UShort_t i=0; i<guis.size(); ++i)
    delete guis[i];
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tf::add(string fname, string pnt, ULong64_t head, string data)
{
	//store filename
	f = fname;

	//check to see if the file is open, sort file, add file to list with
	//referencing pointer, create tree, perform merge-sort, fill tree,
	//and assign referencing pointer to tree in interpreter's scope
	string filetemp = f.substr(f.find_last_of('.')+1,-1);
	for(UShort_t i=0; i<filetemp.length(); ++i)
		tolower(filetemp[i]);

	if(filetemp!="root")
	{
		if(getb(pnt,head,data)==1)
			cout << "\n  Cannot Open File\n\n";
	}
	else
	{
		UChar_t check = 0;
		if((check = getr(pnt))==1)
			cout << "\n  Cannot Open File\n\n";

		if(check==2)
			cout << "\n  There Are No Trees In File\n\n";
	}
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tf::add(string fname, string pnt)
{
	//store filename
	f = fname;

	//check to see if the file is open, sort file, add file to list with
	//referencing pointer, create tree, perform merge-sort, fill tree,
	//and assign referencing pointer to tree in interpreter's scope
	string filetemp = f.substr(f.find_last_of('.')+1,-1);
	for(UShort_t i=0; i<filetemp.length(); ++i)
		tolower(filetemp[i]);

	if(filetemp!="root")
	{
		if(getb(pnt)==1)
			cout << "\n  Cannot Open File\n\n";
	}
	else
	{
		UChar_t check = 0;
		if((check = getr(pnt))==1)
			cout << "\n  Cannot Open File\n\n";

		if(check==2)
			cout << "\n  There Are No Trees In File\n\n";
	}
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tf::draw(string tt, string vars, string cuts, string opts)
{
  TH1::AddDirectory(kFALSE);

	//create temporary pointer to current pad, before tf::draw is used
	TVirtualPad* temp_pad = gPad;

	//instantiate tgui object
	guis.push_back(new tgui);
  tgui* gui = guis.back();

	//delete all spaces, intentional or not, from all arguments
	while((Short_t)tt.find(' ')!=-1)
		tt.erase(tt.find(' '),1);

	while((Short_t)vars.find(' ')!=-1)
		vars.erase(vars.find(' '),1);

	while((Short_t)cuts.find(' ')!=-1)
		cuts.erase(cuts.find(' '),1);

	while((Short_t)opts.find(' ')!=-1)
		opts.erase(opts.find(' '),1);

	//start draw loop
	while(tt.empty()==0 && (vars.empty()==0 || flags[0]==1))
	{
		//get tree pointer
		A = tt.substr(0,tt.find(',')); tt.erase(0,tt.find(','));

		//erase entry from argument
		if(tt.empty()==0)
			tt.erase(0,1);

		//check to see if '~' has been used
		if(flags[0]==0)
		{
			//get variable(s) to be plotted
			B = vars.substr(0,vars.find(',')); vars.erase(0,vars.find(','));

			//erase entry from argument
			if(vars.empty()==0)
				vars.erase(0,1);
		}

		//check to see if '~' has been used
		if(flags[1]==0)
		{
			//check to see if any entries are left in argument
			if(cuts.empty()==0)
			{
				//get cuts for plot
				C = cuts.substr(0,cuts.find(',')); cuts.erase(0,cuts.find(','));

				//erase entry from argument
				if(cuts.empty()==0)
					cuts.erase(0,1);
			}
			else
				C = "";
		}

		//check to see if '~' has been used
		if(flags[2]==0)
		{
			//check to see if any entries are left in argument
			if(opts.empty()==0)
			{
				//get options for plot
				D = opts.substr(0,opts.find(',')); opts.erase(0,opts.find(','));

				//cast letters to uppercase
				Char_t temp;
				for(UChar_t i=0; D[i]; ++i)
				{
					temp = D[i]; D[i] = toupper(temp);
				}

				//erase entry from argument
				if(opts.empty()==0)
					opts.erase(0,1);
			}
			else
				D = "";
		}

		//check for '~' in variable argument
		if((Short_t)B.find('~')!=-1)
		{
			B.erase(B.find('~'),1);

			//set flag 0
			flags[0] = 1;
		}

		//check for '~' in cuts argument
		if((Short_t)C.find('~')!=-1)
		{
			C.erase(C.find('~'),1);

			//set flag 1
			flags[1] = 1;
		}

		//check for '~' in options argument
		if((Short_t)D.find('~')!=-1)
		{
			D.erase(D.find('~'),1);

			//set flags 2 and 8
			flags[2] = 1;
			flags[8] = 1;
		}

		//check for multiple pads in tree argument
		if((Short_t)A.find('(')!=-1)
		{
			//delete '('
			A.erase(A.find('('),1);

			//check for end of group
			if((Short_t)A.find(')')!=-1)
			{
				A.erase(A.find(')'),1);

				//set flag 6
				flags[6] = 1;
			}
			else
			{
				//set flag 10 if appropriate
				if(D=="SAME")
					flags[10] = 1;
			}

			//set flags 4 and 5
			flags[4] = 1;
      flags[5] = 0;

			//create temporary counter
			Short_t ct = -1;

			//start loop to find the extent of the group
			while(flags[6]==0)
			{
				//use flag 5 to sift through tree argument entries
				flags[5]=(Short_t)tt.find(',',flags[5]+1);

				//if ')' is found or there are no more entries, set flag 6 to break loop
				if((Short_t)tt.find(')')<flags[5] || flags[5]==-1)
				{
					tt.erase(tt.find(')'),1);
					//set flag 6
					flags[6] = 1;
				}

				//create temps for option entry
				string tempp; Char_t temp;

				//check to see if end of argument has been reached
				if(ct!=-1 || flags[9]==0)
				{
					//set flag 9, store option entry and cast it to uppercase
					++flags[9];
					tempp = opts.substr(ct+1,(UShort_t)opts.find(',',ct+1)-ct-1);
					ct = opts.find(',',ct+1);
					for(UChar_t i=0; tempp[i]; ++i)
					{
						temp = tempp[i]; tempp[i] = toupper(temp);
					}

					//check for '~'
					if((Short_t)tempp.find('~')!=-1)
					{
						tempp.erase(tempp.find('~'),1);

						//set flag 8
						flags[8] = 1;
					}
				}
				else
					tempp = "";

				//use flag 7 to count the number of SAME options in group
				if(tempp=="SAME"||flags[8]==1)
					++flags[7];

				//use flag 4 to count the number of tree pointers in group
				++flags[4];
			}

			//reset flags 5, 6, and 9
			flags[5] = 0;
			flags[6] = 0;
			flags[9] = 0;
		}

		//check to see if multi-pad tab is needed
		if((flags[4]-flags[7])>1 && flags[5]<(flags[4]-flags[7]))
		{
			if(flags[5]==0)
				//make new set of pads and change to first
				gui->tab(flags[4]-flags[7]);
			else
			{
				//change to next pad if SAME option is not used
				if(D!="SAME" || flags[3]==0)
					gui->swap(flags[5]+1);
			}

			//use flag 5 to count the number of pads used
			if(D!="SAME" || flags[5]==0)
				++flags[5];

			//check for last pad
			if(!(flags[5]<(flags[4]-flags[7])))
			{
				//reset flags 4 and 7
				flags[4] = 0;
				flags[7] = 0;
			}
		}
		else
		{
			if(D!="SAME" || flags[3]==0 || flags[7]>0)
				//create tab with one pad
				gui->tab(1);

			//reset flags 4 and 7
			flags[4] = 0;
			flags[7] = 0;
		}

		//assign member pointer, t_, to tree pointer in argument
		set_tree();

		//create temporary holder for histemp index
		UChar_t w = 0;

		//check to see if SAME option was provided for first entry in tree argument, or
		//for first entry in group (which would be incorrect)
		if((D=="SAME" && flags[3]==0) || flags[10]==1)
		{
			//check to see if plot is TH1 or TGraph
			if((Short_t)B.find(':')==-1)
			{
	    	//draw tree
        t_->SetEstimate(-1);
	    	t_->Draw(B.c_str(),C.c_str(),"goff");

        Float_t temph = t_->GetV1()[0];
        Float_t templ = t_->GetV1()[0];
        for(Long64_t i=1; i<t_->GetSelectedRows(); ++i)
        {
          if(t_->GetV1()[i]>temph)
            temph = t_->GetV1()[i];

          if(t_->GetV1()[i]<templ)
            templ = t_->GetV1()[i];
        }

				//make copy of TTree::Draw histogram
				histemp[0] = new TH1F("histemp","hist",100,templ,temph+0.05*(temph-templ));

        histemp[0]->FillN(t_->GetSelectedRows(),t_->GetV1(),NULL);

				//set styling attributes and draw
				histemp[0]->SetLineWidth(2);
				histemp[0]->SetLineColor(gui->leg->GetNRows());
				histemp[0]->SetMarkerStyle(6);
				histemp[0]->SetMarkerColor(gui->leg->GetNRows());
				histemp[0]->Draw();

        t_->SetEstimate(1000000);
			}
			else
			{
		  	//draw tree without options entry
			  t_->Draw(B.c_str(),C.c_str(),"goff");

				//make copy of TTree::Draw graph
        if(t_->GetSelectedRows()<1000000)
			  	gratemp = new TGraph(t_->GetSelectedRows(),t_->GetV2(),t_->GetV1());
        else
         	gratemp = new TGraph(1000000,t_->GetV2(),t_->GetV1());

				//set styling attributes and draw
				gratemp->SetLineWidth(2);
				gratemp->SetLineColor(gui->leg->GetNRows());
				gratemp->SetMarkerStyle(6);
				gratemp->SetMarkerColor(gui->leg->GetNRows());
				gratemp->Draw("P");
			}
		}
		else
		{
			//check to see if options argument is empty
			if(opts.empty()==0)
			{
				//store next option entry
				F = opts.substr(0,opts.find(','));

				//cast letters to uppercase
				Char_t temp;
				for(UChar_t i=0; F[i]; ++i)
				{
					temp = F[i]; F[i] = toupper(temp);
				}

				//erase '~' if its there
				if((Short_t)F.find('~')!=-1)
					F.erase(F.find('~'),1);
			}

			//check for SAME option
			if(D=="SAME" || F=="SAME")
			{
				//check to see if plot is TH1 or TGraph
				if((Short_t)B.find(':')==-1)
				{
		    	//draw tree
          t_->SetEstimate(-1);
		    	t_->Draw(B.c_str(),C.c_str(),"goff");

          Float_t temph = t_->GetV1()[0];
          Float_t templ = t_->GetV1()[0];
          for(Long64_t i=1; i<t_->GetSelectedRows(); ++i)
          {
            if(t_->GetV1()[i]>temph)
              temph = t_->GetV1()[i];

            if(t_->GetV1()[i]<templ)
              templ = t_->GetV1()[i];
          }

					//check to see if this is first plot for current pad
					if(gui->leg->GetNRows()==1)
          {
						//make copy of TTree::Draw histogram
						histemp[0] = new TH1F("histemp","hist",100,templ,temph+0.05*(temph-templ));

            histemp[0]->FillN(t_->GetSelectedRows(),t_->GetV1(),NULL);
          }
					else
					{
						//make copy of TTree::Draw histogram
			    	histemp[1] = new TH1F("histemp","hist",100,templ,temph+0.05*(temph-templ));

            histemp[1]->FillN(t_->GetSelectedRows(),t_->GetV1(),NULL);

						//set maximum for axis of first plot if this plot is larger
						if(histemp[1]->GetMaximum()>histemp[0]->GetMaximum())
							histemp[0]->SetMaximum(histemp[1]->GetMaximum()*1.1);
						w = 1;
					}

					//set styling attributes and draw
					histemp[w]->SetLineWidth(2);
					histemp[w]->SetLineColor(gui->leg->GetNRows());
					histemp[w]->SetMarkerStyle(6);
					histemp[w]->SetMarkerColor(gui->leg->GetNRows());
          if(D=="SAME")
					  histemp[w]->Draw("SAME");
          else
            histemp[w]->Draw();

          t_->SetEstimate(1000000);
				}
				else
				{
		     	//draw tree
		    	t_->Draw(B.c_str(),C.c_str(),"goff");

					//make copy of TTree::Draw graph
          if(t_->GetSelectedRows()<1000000)
			     	gratemp = new TGraph(t_->GetSelectedRows(),t_->GetV2(),t_->GetV1());
          else
           	gratemp = new TGraph(1000000,t_->GetV2(),t_->GetV1());

					//set styling attributes and draw
					gratemp->SetLineWidth(2);
					gratemp->SetLineColor(gui->leg->GetNRows());
					gratemp->SetMarkerStyle(6);
					gratemp->SetMarkerColor(gui->leg->GetNRows());
          if(D=="SAME")
					  gratemp->Draw("SAME AP");
          else
            gratemp->Draw("AP");
				}
			}
      else
	  		//draw tree
		  	t_->Draw(B.c_str(),C.c_str(),D.c_str());
		}

		//add spaces back to cuts argument in appropriate places
		for(Short_t i=0, j=0; (i=C.find("&&",j))!=-1 && j!=-1;)
		{
			if(C[i-1]!=' ')
			{
				C.insert(i," ");
				C.insert((j=i+3)," ");
			}
			else
				j = -1;
		}

		for(Short_t i=0, j=0; (i=C.find("||",j))!=-1 && j!=-1;)
		{
			if(C[i-1]!=' ')
			{
				C.insert(i," ");
				C.insert((j=i+3)," ");
			}
			else
				j = -1;
		}

		//create string(s) with cuts entry and add to displayed cuts box
		for(UShort_t i=0; i<files[0].size(); ++i)
		{
			if(files[0][i]==A)
			{
				gui->cutbox->AddText("");

				if((Short_t)files[1][i].find_last_of('/',files[1][i].length())!=-1)
					E = files[1][i].substr(files[1][i].find_last_of('/',files[1][i].length())+1,-1) + ": " + C;
				else
					E = files[1][i] + ": " + C;

				if(E.length()>23)
				{
					gui->cutbox->AddText(E.substr(0,23).c_str());

					if(E.length()>46)
					{
						gui->cutbox->AddText(E.substr(23,23).c_str());
						gui->cutbox->AddText(E.substr(46,-1).c_str());
					}
					else
						gui->cutbox->AddText(E.substr(23,-1).c_str());
				}
				else
					gui->cutbox->AddText(E.c_str());
			}
		}

		//create string with file name and number of entries in plot, with '#splitline' tag
		for(UShort_t i=0; i<files[0].size(); ++i)
		{
			if(files[0][i]==A)
			{
				if((Short_t)files[1][i].find_last_of('/',files[1][i].length())!=-1)
					E = files[1][i].substr(files[1][i].find_last_of('/',files[1][i].length())+1,-1);
				else
					E = files[1][i];

				Char_t Etemp[150] = {' '};
				sprintf(Etemp,"#splitline{%s: %s}{Entries: %lld}",E.c_str(),B.c_str(),t_->GetEntries());
				E = "";
				for(UChar_t j=0, k=0, l=0; j<150; ++j)
				{
					if(Etemp[149-j]!=' ' && k==0)
						l=149-j;

					if(l>0 && k<l+1)
					{
						E += Etemp[k];
						++k;
					}
				}
			}
		}

		//add previously created string to legend
		if(D=="SAME" || F=="SAME")
		{
			if((Short_t)B.find(':')==-1)
				gui->leg->AddEntry(histemp[w],E.c_str(),"l");
			else
				gui->leg->AddEntry(gratemp,E.c_str(),"l");
		}
		else
		{
			if((Short_t)B.find(':')==-1)
				gui->leg->AddEntry(t_,E.c_str(),"l");
			else
				gui->leg->AddEntry(t_,E.c_str(),"l");
		}

		//set flag 3 to indicate that first pass through draw loop is complete
		++flags[3];

		//reset flag 10 and F string
		flags[10] = 0;
		F = "";
	}

	//display plots
	gui->display();

	//reset all flags
	for(UChar_t i=0; i<11; ++i)
		flags[i] = 0;

	//change back to previously used canvas if one exists
	if(temp_pad!=NULL)
		temp_pad->cd();

  TH1::AddDirectory(kTRUE);
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tf::draw(string tt, string vars, string cuts)
{
	//create temporary pointer to current pad, before tf::draw is used
	TVirtualPad* temp_pad = gPad;

	//instantiate tgui object
	guis.push_back(new tgui);
  tgui* gui = guis.back();

	//delete all spaces, intentional or not, from all arguments
	while((Short_t)tt.find(' ')!=-1)
		tt.erase(tt.find(' '),1);

	while((Short_t)vars.find(' ')!=-1)
		vars.erase(vars.find(' '),1);

	while((Short_t)cuts.find(' ')!=-1)
		cuts.erase(cuts.find(' '),1);

	//start draw loop
	while(tt.empty()==0 && (vars.empty()==0 || flags[0]==1))
	{
		//get tree pointer
		A = tt.substr(0,tt.find(',')); tt.erase(0,tt.find(','));

		//erase entry from argument
		if(tt.empty()==0)
			tt.erase(0,1);

		//check to see if '~' has been used
		if(flags[0]==0)
		{
			//get variable(s) to be plotted
			B = vars.substr(0,vars.find(',')); vars.erase(0,vars.find(','));

			//erase entry from argument
			if(vars.empty()==0)
				vars.erase(0,1);
		}

		//check to see if '~' has been used
		if(flags[1]==0)
		{
			//check to see if any entries are left in argument
			if(cuts.empty()==0)
			{
				//get cuts for plot
				C = cuts.substr(0,cuts.find(',')); cuts.erase(0,cuts.find(','));

				//erase entry from argument
				if(cuts.empty()==0)
					cuts.erase(0,1);
			}
			else
				C = "";
		}

		//check for '~' in variable argument
		if((Short_t)B.find('~')!=-1)
		{
			B.erase(B.find('~'),1);

			//set flag 0
			flags[0] = 1;
		}

		//check for '~' in variable argument
		if((Short_t)C.find('~')!=-1)
		{
			C.erase(C.find('~'),1);

			//set flag 1
			flags[1] = 1;
		}

		//check for multiple pads in tree argument
		if((Short_t)A.find('(')!=-1)
		{
			//delete '('
			A.erase(A.find('('),1);

			//check for end of group
			if((Short_t)A.find(')')!=-1)
			{
				A.erase(A.find(')'),1);

				//set flag 6
				flags[6] = 1;
			}
			
			//set flag 4 and 5
			flags[4] = 1;
			flags[5] = 0;

			//start loop to find the extent of the group
			while(flags[6]==0)
			{
				//use flag 5 to sift through tree argument entries
				flags[5]=(Short_t)tt.find(',',flags[5]+1);

				//if ')' is found or there are no more entries, set flag 6 to break loop
				if((Short_t)tt.find(')')<flags[5] || flags[5]==-1)
				{
					tt.erase(tt.find(')'),1);
					//set flag 6
					flags[6] = 1;
				}

				//use flag 4 to count the number of tree pointers in group
				++flags[4];
			}

			//reset flags 5 and 6
			flags[5] = 0;
			flags[6] = 0;
		}

		//check to see if multi-pad tab is needed
		if(flags[4]>1 && flags[5]<flags[4])
		{
			if(flags[5]==0)
				//make new set of pads and change to first
				gui->tab(flags[4]);
			else
				//change to next pad
				gui->swap(flags[5]+1);

			//use flag 5 to count the number of pads used
			++flags[5];
		}
		else
		{
			//create tab with one pad
			gui->tab(1);

			//reset flag 4
			flags[4] = 0;
		}

		//assign member pointer, t_, to tree pointer in argument and draw
		set_tree();
		t_->Draw(B.c_str(),C.c_str());

		//add spaces back to cuts argument in appropriate places
		for(Short_t i=0, j=0; (i=C.find("&&",j))!=-1 && j!=-1;)
		{
			if(C[i-1]!=' ')
			{
				C.insert(i," ");
				C.insert((j=i+3)," ");
			}
			else
				j = -1;
		}

		for(Short_t i=0, j=0; (i=C.find("||",j))!=-1 && j!=-1;)
		{
			if(C[i-1]!=' ')
			{
				C.insert(i," ");
				C.insert((j=i+3)," ");
			}
			else
				j = -1;
		}

		//create string(s) with cuts entry and add to displayed cuts box
		for(UShort_t i=0; i<files[0].size(); ++i)
		{
			if(files[0][i]==A)
			{
				gui->cutbox->AddText("");

				if((Short_t)files[1][i].find_last_of('/',files[1][i].length())!=-1)
					E = files[1][i].substr(files[1][i].find_last_of('/',files[1][i].length())+1,-1) + ": " + C;
				else
					E = files[1][i] + ": " + C;

				if(E.length()>23)
				{
					gui->cutbox->AddText(E.substr(0,23).c_str());

					if(E.length()>46)
					{
						gui->cutbox->AddText(E.substr(23,23).c_str());
						gui->cutbox->AddText(E.substr(46,-1).c_str());
					}
					else
						gui->cutbox->AddText(E.substr(23,-1).c_str());
				}
				else
					gui->cutbox->AddText(E.c_str());
			}
		}

		//create string with file name and number of entries in plot, with '#splitline' tag
		for(UShort_t i=0; i<files[0].size(); ++i)
		{
			if(files[0][i]==A)
			{
				if((Short_t)files[1][i].find_last_of('/',files[1][i].length())!=-1)
					E = files[1][i].substr(files[1][i].find_last_of('/',files[1][i].length())+1,-1);
				else
					E = files[1][i];

				Char_t Etemp[150] = {' '};
				sprintf(Etemp,"#splitline{%s: %s}{Entries: %lld}",E.c_str(),B.c_str(),t_->GetEntries());
				E = "";
				for(UChar_t j=0, k=0, l=0; j<150; ++j)
				{
					if(Etemp[149-j]!=' ' && k==0)
						l=149-j;

					if(l>0 && k<l+1)
					{
						E += Etemp[k];
						++k;
					}
				}
			}
		}

		//add previously created string to legend
		if((Short_t)B.find(':')==-1)
			gui->leg->AddEntry(t_,E.c_str(),"l");
		else
			gui->leg->AddEntry(t_,E.c_str(),"l");
	}

	//display plots
	gui->display();

	//reset all flags
	flags[0] = 0; flags[1] = 0; flags[4] = 0;
	flags[5] = 0; flags[6] = 0;

	//change back to previously used canvas if one exists
	if(temp_pad!=NULL)
		temp_pad->cd();
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tf::draw(string tt, string vars)
{
  draw(tt, vars, "");
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tf::sys_info()
{
  #ifdef __linux__
  struct sysinfo info;
  sysinfo(&info);

  Double_t fgbytes = (Double_t)info.mem_unit*info.freeram/1024/1024/1024;
  Double_t tgbytes = (Double_t)info.mem_unit*info.totalram/1024/1024/1024;

  cout << setprecision(4);
  cout << "Total Memory: " << tgbytes << " GB, ";
  cout << "Available Memory: " << fgbytes << " GB\n";
  #else
  cout << "This function only works with a Linux kernel.\n";
  #endif
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


UChar_t tf::getb(string pnt, ULong64_t head, string data)
{
	//open binary file
	ifstream fin(f, ios::binary);

  f.erase(f.find_last_of('.'));

	//determine if file was opened
	if(fin.is_open())
	{
    //create tree names
    A = "run_tree";
    B = "data";

    if(n<10)
    {
	    A += (Char_t)(n+48);
	    B += (Char_t)(n+48);
    }
    else
    {
	    A += '1' + (Char_t)(n+38);
	    B += '1' + (Char_t)(n+38);
    }

	  //create branch buffers
    vector<ULong64_t> ulongb;
    vector<Long64_t> slongb;
    vector<Double_t> doubleb;
    vector<Char_t> ulongbt;
    vector<Char_t> slongbt;
    vector<Char_t> doublebt;
    vector<UChar_t> datasz;

    //create tree
    TTree* temptree = new TTree(A.c_str(),B.c_str());

	  while((Short_t)data.find(' ')!=-1)
		  data.erase(data.find(' '),1);

    for(UShort_t i=0; data.empty()==0; ++i)
    {
		  switch(data[0])
      {
        case 'b':
          datasz.push_back(1); 
          ulongb.push_back(i);
          ulongbt.push_back(data[0]);
          break;

        case 's':
          datasz.push_back(2); 
          ulongb.push_back(i);
          ulongbt.push_back(data[0]);
          break;

        case 'i':
          datasz.push_back(4); 
          ulongb.push_back(i);
          ulongbt.push_back(data[0]);
          break;

        case 'l':
          datasz.push_back(8); 
          ulongb.push_back(i);
          ulongbt.push_back(data[0]);
          break;

        case 'B':
          datasz.push_back(1); 
          slongb.push_back(i);
          slongbt.push_back(data[0]);
          break;

        case 'S':
          datasz.push_back(2); 
          slongb.push_back(i);
          slongbt.push_back(data[0]);
          break;

        case 'I':
          datasz.push_back(4); 
          slongb.push_back(i);
          slongbt.push_back(data[0]);
          break;

        case 'L':
          datasz.push_back(8); 
          slongb.push_back(i);
          slongbt.push_back(data[0]);
          break;

        case 'F':
          datasz.push_back(4);
          doubleb.push_back(i);
          doublebt.push_back(data[0]);
          break;

        case 'D':
          datasz.push_back(8);
          doubleb.push_back(i);
          doublebt.push_back(data[0]);
      }

      data.erase(0,1);
      if(data.empty()==0)
        data.erase(0,1);
    }

	  //create branches
    for(UShort_t i=0, j=0, k=0; i+j+k<(Short_t)(ulongb.size()+slongb.size()+doubleb.size());)
    {
      string temp; temp += (Char_t)(i + j + k +97);
      string temp2;
      UChar_t flag = 0;

      for(UShort_t l=0; l<ulongb.size(); ++l)
      {
        if((Short_t)ulongb[l]==i+j+k)
          flag = 1;
      }

      for(UShort_t l=0; l<slongb.size(); ++l)
      {
        if(slongb[l]==i+j+k)
          flag = 2;
      }

      for(UShort_t l=0; l<doubleb.size(); ++l)
      {
        if((Short_t)doubleb[l]==i+j+k)
          flag = 3;
      }

      switch(flag)
      {
        case 1:
          temp2 = temp + '/' + ulongbt[i];
          temptree->Branch(temp.c_str(),&ulongb[i],temp2.c_str());
          ++i; break;

        case 2:
          temp2 = temp + '/' + slongbt[j];
          temptree->Branch(temp.c_str(),&slongb[j],temp2.c_str());
          ++j; break;

        case 3:
          temp2 = temp + '/' + doublebt[k];
          temptree->Branch(temp.c_str(),&doubleb[k],temp2.c_str());
          ++k;
      }
    }

    files[0].push_back(pnt);
    files[1].push_back(f);

		fin.seekg(head);

		//read data into tree
    while(fin.eof()==0)
    {
      string temp;

      for(UChar_t i=0; i<datasz.size(); ++i)
      {
        temp += (Char_t)(i+97);

        fin.read((Char_t*)temptree->GetBranch(temp.c_str())->GetAddress(),datasz[i]);

        temp.clear();
      }

      temptree->Fill();
    }

    t_ = temptree->CloneTree();

    delete temptree;

    A = "TTree* " + pnt + " = tf::t_;";
    gROOT->ProcessLine(A.c_str());

    //add to counter (used for naming)
    ++n;

		return 0;
	}
	else
		return 1;
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


UChar_t tf::getb(string pnt)
{
	//open binary file
	ifstream fin(f, ios::binary);

  f.erase(f.find_last_of('.'));

	//determine if file was opened
	if(fin.is_open())
	{
    //create tree names
    A = "run_tree";
    B = "data";

    if(n<10)
    {
	    A += (Char_t)(n+48);
	    B += (Char_t)(n+48);
    }
    else
    {
	    A += '1' + (Char_t)(n+38);
	    B += '1' + (Char_t)(n+38);
    }

    //create tree
    TTree* temptree = new TTree(A.c_str(),B.c_str());

	  //create branch buffers
    UInt_t board;
	  UChar_t channel;
	  ULong64_t timestamp;
	  UShort_t adc;

	  //create branches
	  temptree->Branch("bdch", &board, "bc/i");
	  temptree->Branch("timestamp", &timestamp, "t/l");
	  temptree->Branch("adc", &adc, "e/s");

    files[0].push_back(pnt);
    files[1].push_back(f);

		fin.seekg(8+15000);

		//read data into tree
		while(fin.read((Char_t*)&board,4) && fin.read((Char_t*)&channel,1) &&
		fin.read((Char_t*)&timestamp,8) && fin.read((Char_t*)&adc,2))
		{
			board = 8*board + channel;
			temptree->Fill();
		}

    t_ = temptree->CloneTree();

    delete temptree;

    A = "TTree* " + pnt + " = tf::t_;";
    gROOT->ProcessLine(A.c_str());

    //add to counter (used for naming)
    ++n;

		return 0;
	}
	else
		return 1;
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


UChar_t tf::getr(string pnt)
{
	UShort_t cnt = 0;

	TDirectory* dir = gDirectory;

	TFile file(f.c_str());

  f.erase(f.find_last_of('.'));

	dir->cd();

	if(file.IsZombie()==0)
	{
		TIter keys(file.GetListOfKeys());
		TKey *k = NULL;

		while((k = (TKey*)keys()))
		{
			if(strcmp(k->GetClassName(),"TTree")==0)
				++cnt;
		}
		keys.Reset();

		if(cnt==0)
			return 2;

		for(Short_t i=0; (k = (TKey*)keys());)
		{
			if(strcmp(k->GetClassName(),"TTree")==0)
			{
				TTree* temptree = (TTree*)file.Get(k->GetName());
				t_ = temptree->CloneTree();

				if(cnt==1)
				{
					files[0].push_back(pnt);
					files[1].push_back(f);

					A = "TTree* " + pnt + " = tf::t_;";
					gROOT->ProcessLine(A.c_str());
				}
				else
				{
					Char_t* ctemp = new Char_t[pnt.length()+6];
					sprintf(ctemp,"%s_%i",pnt.c_str(),i);

					files[0].push_back(ctemp);
					files[1].push_back(f+'_'+k->GetName());

					A = "TTree* ";
					for(UShort_t j=0; j<strlen(ctemp); ++j)
						A += ctemp[j];
					A += " = tf::t_;";
					gROOT->ProcessLine(A.c_str());

					delete[] ctemp;
					++i;
				}
			}
		}

		if(cnt!=1)
			cout << "Number of trees: " << cnt << "  (Append '_i' to provided pointer name, where i is the index.)\n";

		return 0;
	}
	else
		return 1;
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tf::sort(string pnt, string bname, UChar_t opt)
{
  A = pnt;
  set_tree();

  Long64_t nent = t_->GetEntries();
  Long64_t *index = new Long64_t[nent];

  t_->SetEstimate(nent+1);
  t_->Draw(bname.c_str(),"","goff");

  if(opt==0)
    TMath::Sort(nent, t_->GetV1(), index, kFALSE);
  else
    TMath::Sort(nent, t_->GetV1(), index);

  TTree *tsort = t_->CloneTree(0);

  for(Long64_t i=0; i<nent; ++i)
  {
    t_->GetEntry(index[i]);
    tsort->Fill();
  }

  delete t_;
  delete[] index;

  t_ = tsort;

	A = pnt + " = tf::t_;";
	gROOT->ProcessLine(A.c_str());
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tf::sort(string pnt, string bname)
{
  sort(pnt, bname, 0);
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tf::set_tree()
{
	//set tf TTree pointer to argument entry from draw function
	string Atemp = "tf::t_ = " + A + ";";
	gROOT->ProcessLine(Atemp.c_str());
}
