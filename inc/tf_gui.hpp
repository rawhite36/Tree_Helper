//Created by Ryan Whitehead, 07-10-16
//tgui member definitions
//ROOT Graphics Handler


#ifndef tree_gui_H
#define tree_gui_H

#include <TGFrame.h>
#include <TGTab.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TVirtualPad.h>
#include <TLegend.h>
#include <TPaveText.h>
#include <TGButton.h>
#include <TStyle.h>
#include <TGClient.h>
#include <RQ_OBJECT.h>
#include "tf_gui_hd.hpp"

using namespace std;


tgui::tgui()
{
	mainf = new TGMainFrame(gClient->GetRoot(),100,100);
	mainf->Connect("CloseWindow()","tgui",this,"clean()");

	TGTextButton* sbutton = new TGTextButton(mainf,"&Save");
	sbutton->Connect("Clicked()","tgui",this,"save()");
	mainf->AddFrame(sbutton, new TGLayoutHints(kLHintsNormal,2,2,2,2));

	tabs = new TGTab(mainf,100,100);

	vfr = NULL; leg = NULL; cutbox = NULL;
  pads = 0; closed = 0;
	n = 1;

	gStyle->SetPadRightMargin(0.24);
	gStyle->SetPadTopMargin(0.05);
	gStyle->SetOptTitle(kFALSE);
	gStyle->SetOptStat(0);
	gStyle->SetTextFont(82);
	gStyle->SetLegendTextSize(0.025);
	gStyle->SetLegendFont(82);
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


tgui::~tgui()
{
  if(closed==0)
  {
    clean();
    delete mainf;
  }
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::clean()
{
  closed = 1;

  for(UChar_t i=0; i<can.size(); ++i)
  {
    UChar_t npads = 0;
    TIter subpads(can[i]->GetCanvas()->GetListOfPrimitives());
    TObject *sp = NULL;

    while((sp = subpads()))
    {
      if(sp->InheritsFrom(TVirtualPad::Class()))
        ++npads;
    }

    if(npads==0)
      can[i]->GetCanvas()->GetListOfPrimitives()->SetOwner();

    if(npads==2)
    {
      TIter subpads2(can[i]->GetCanvas()->GetPad(1)->GetListOfPrimitives());
      TObject *sp2 = NULL;
      UChar_t flag = 0;

      while((sp2 = subpads2()))
      {
        if(sp2->InheritsFrom(TVirtualPad::Class()))
          flag = 1;
      }

      if(flag==0)
      {
        can[i]->GetCanvas()->GetPad(1)->GetListOfPrimitives()->SetOwner();
        can[i]->GetCanvas()->GetPad(2)->GetListOfPrimitives()->SetOwner();
      }
      else
      {
        can[i]->GetCanvas()->GetPad(1)->GetPad(1)->GetListOfPrimitives()->SetOwner();
        can[i]->GetCanvas()->GetPad(1)->GetPad(2)->GetListOfPrimitives()->SetOwner();
        can[i]->GetCanvas()->GetPad(2)->GetPad(1)->GetListOfPrimitives()->SetOwner();
      }
    }

    if(npads==4)
    {
      for(UChar_t j=0; j<npads; ++j)
        can[i]->GetCanvas()->GetPad(j+1)->GetListOfPrimitives()->SetOwner();
    }
  }

  mainf->SetCleanup(kDeepCleanup);
	mainf->Cleanup();
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::save()
{
	gErrorIgnoreLevel = 1001;

 	time_t tim = time(0);
	string tims = ctime(&tim);
  string temp;
	
	tims.erase(0,4);
	while((Short_t)tims.find(' ')!=-1 && (Short_t)tims.find(' ')<4)
		tims.erase(tims.find(' '),1);

	while((Short_t)tims.find(' ')!=-1)
		tims.replace(tims.find(' '),1,"_");

  tims.resize(tims.length()-1);

	for(UShort_t i=0; i<can.size(); ++i)
	{
	  if(i<10)
      temp = "saved/" + A + "_" + (Char_t)(i+48) + "_" + tims + ".gif";
	  else
      temp = "saved/" + A + "_1" + (Char_t)(i+38) + "_" + tims + ".gif";

		can[i]->GetCanvas()->Print(temp.c_str(),"gif");
	}

	gErrorIgnoreLevel = -1;
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::tab(UChar_t num)
{
	A = "Tab ";

	if(n<10)
		A += (Char_t)(n+48);
	else
	{
		A += '1';
		A += (Char_t)(n+38);
	}

	if(n!=1)
	{
		leg->SetY1(0.99-0.075*leg->GetNRows());
		leg->Draw();
		cutbox->SetY1(leg->GetY1()-0.031*cutbox->GetSize());
		cutbox->SetY2(leg->GetY1());
		cutbox->Draw();
	}

	vfr = new TGVerticalFrame(tabs,100,100);
	B = "can" + A;
	can.push_back(new TRootEmbeddedCanvas(B.c_str(),vfr,900,600));
	vfr->AddFrame(can.back(),new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

	switch(pads=num)
	{
		case 2:
			can.back()->GetCanvas()->Divide(1,2,0.001,0.001);
			can.back()->GetCanvas()->cd(1);
			break;
		
		case 3:
			can.back()->GetCanvas()->Divide(1,2,0.001,0.001);
      can.back()->GetCanvas()->GetPad(1)->Divide(2,1,0.001,0.001);
      can.back()->GetCanvas()->GetPad(2)->Divide(1,1,0.25,0.001);
			can.back()->GetCanvas()->GetPad(1)->cd(1);
			break;
		
		case 4:
			can.back()->GetCanvas()->Divide(2,2,0.001,0.001);
			can.back()->GetCanvas()->cd(1);
	}

	leg = new TLegend(0.76,0.93,0.999,0.999,"Legend");
	leg->SetTextAlign(22);

	cutbox = new TPaveText(0.76,0.87,0.999,0.93,"NDC");
	cutbox->SetBorderSize(1);
	cutbox->SetFillColor(0);
	cutbox->SetTextSize(0.025);
	cutbox->AddText("");
	cutbox->AddText("Cuts");

	tabs->AddTab(A.c_str(),vfr);
	++n;
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::swap(UChar_t num)
{
	leg->SetY1(0.99-0.075*leg->GetNRows());
	leg->Draw();
	cutbox->SetY1(leg->GetY1()-0.031*cutbox->GetSize());
	cutbox->SetY2(leg->GetY1());
	cutbox->Draw();

	switch(num)
	{
		case 2:
      if(pads==3)
        can.back()->GetCanvas()->GetPad(1)->cd(2);
      else
			  can.back()->GetCanvas()->cd(2);
			break;

		case 3:
			if(pads==3)
				can.back()->GetCanvas()->GetPad(2)->cd(1);
			else
				can.back()->GetCanvas()->cd(3);
			break;

		case 4:
			can.back()->GetCanvas()->cd(4);
	}

	leg = new TLegend(0.76,0.93,.999,0.999,"Legend");
	leg->SetTextAlign(22);

	cutbox = new TPaveText(0.76,0.87,0.999,0.93,"NDC");
	cutbox->SetBorderSize(1);
	cutbox->SetFillColor(0);
	cutbox->SetTextSize(0.025);
	cutbox->AddText("");
	cutbox->AddText("Cuts");
}


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


void tgui::display()
{
	leg->SetY1(0.99-0.075*leg->GetNRows());
	leg->Draw();
	cutbox->SetY1(leg->GetY1()-0.031*cutbox->GetSize());
	cutbox->SetY2(leg->GetY1());
	cutbox->Draw();

	A = "Set ";
	if(tf::nn<10)
		A += (Char_t)(tf::nn+48);
	else
	{
		A += '1';
		A += (Char_t)(tf::nn+38);
	}

	mainf->AddFrame(tabs,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));
	mainf->SetWindowName(A.c_str());
	mainf->MapSubwindows();
	mainf->Resize(mainf->GetDefaultSize());
	mainf->MapWindow();

	A.erase(3,1);
	++tf::nn;
}

#endif
