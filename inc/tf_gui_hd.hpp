//Created by Ryan Whitehead, 07-10-16
//tgui class header


#ifndef tree_gui_hd_H
#define tree_gui_hd_H


class tgui
{
	RQ_OBJECT("tgui");

	TGMainFrame* mainf;
	TGTab* tabs;
	TGVerticalFrame* vfr;
	vector<TRootEmbeddedCanvas*> can;
	string A;
	string B;
	UChar_t n;
	UChar_t pads;
  UChar_t closed;

	public:
		TLegend* leg;
		TPaveText* cutbox;

		tgui();
		~tgui();
		void clean();
		void save();
		void tab(UChar_t);
		void swap(UChar_t);
		void display();
};

#endif
