//Created by Ryan Whitehead, 07-10-16
//tf class header


#ifndef tree_filler_H
#define tree_filler_H


class tgui;

class tf
{
	UChar_t getb(string);
  UChar_t getb(string,ULong64_t,string);
	UChar_t getr(string);
	void set_tree();

	UChar_t n;
	Short_t flags[11];
	vector<string> files[2];
	string f;
  string o;
	string A;
	string B;
	string C;
	string D;
	string E;
	string F;
	TH1F* histemp[2];
	TGraph* gratemp;
  vector<tgui*> guis;

	public:
    tf();
		tf(string,string);
    tf(string,string,ULong64_t,string);
		~tf();
    void add(string,string,ULong64_t,string);
		void add(string,string);
  	void sort(string,string,UChar_t);
  	void sort(string,string);
		void draw(string,string,string,string);
		void draw(string,string,string);
		void draw(string,string);
    void sys_info();

    static TTree* t_;
		static UChar_t nn;
};

#endif
