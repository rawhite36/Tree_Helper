


System Details:
===============

OS: Ubuntu v14.04

ROOT: v6.06.04

gcc: v5.3.0

The software has been tested with the above system specifications, but should work for other
versions of ROOT 6 (and possibly ROOT 5). Furthermore, no c++11 syntax is used, so earlier
gcc versions should work as well; although if you compile and create a library with ACLiC,
there will be a compiler mismatch warning if the gcc and ROOT versions don't agree. The code
will still compile, however, there may be unexpected errors while using it, as it has not
been tested with such a mismatch.


Included Files:
===============

tf_class.cpp

inc/

tf_class.hpp

tf_gui.hpp

tf_gui_hd.hpp

saved/

(saved plots are stored here)

data/

(you may store data files here if you wish)


Class tf: Public Member Functions
=================================

tf::tf()

tf::tf(string file_name, string pointer_name)

tf::tf(string file_name, string pointer_name, ULong64_t header_size, string data_structure)

void tf::add(string file_name, string pointer_name)

void tf::add(string file_name, string pointer_name, ULong64_t header_size, string data_structure)

void tf::sort(string pointer_name, string branch/expression, UChar_t asc/dec_opt == 0 (asc))

void tf::draw(string pointer_list, string branch/leaf_list, string cuts == "", string options == "")

void tf::sys_info()


Loading/Compiling:
==================

Simply load tf_class.cpp via '.L' or, if you wish to compile and create a library with ACLiC, you
may append '+' or '++' to the file name. A single '+' creates a library only if one doesn't already
exist, while '++' creates a library every time it is used.


-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------


Public Member Descriptions
==========================


tf::tf(string file_name, string pointer_name)
---------------------------------------------


You only need to instantiate one object (class type: tf), although more than one is fine.


*file_name:*

Give the name of the file. If the suffix is .root then all TTrees will be extracted
from the TFile. Otherwise, a typical binary format is assumed and the data structure
from the Ca-45 runs at LANL will be used. The board and channel will be used to
compute the pixel number, and the first 1000 entries are dropped since the initial
entries from the DAQ cannot be trusted.

*pointer_name:*

Give the name of the pointer you would like to use to refer to the TTree(s). If a
TFile is used that contains more than one tree, then '_i' will be appended to the
pointer name, where i is the index (beginning with 0).


-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------


tf::tf(string file_name, string pointer_name, ULong64_t header_size, string data_structure)
-------------------------------------------------------------------------------------------


*file_name:*

This is the same as above, however the data structure of the binary file will not be assumed.

*pointer_name:*

*header_size:*

Enter the size of the header in bytes.

*data_structure:*

Give the data structure of the file, using the letters listed below (use commas to separate).
A branch will be created for each letter, and is given the name 'a' for the first, and so on
in alphabetical order. These branch names can be changed later via the TBranch::SetName() method.


*Data Types:*

B : an 8 bit signed integer (Char_t)
b : an 8 bit unsigned integer (UChar_t)
S : a 16 bit signed integer (Short_t)
s : a 16 bit unsigned integer (UShort_t)
I : a 32 bit signed integer (Int_t)
i : a 32 bit unsigned integer (UInt_t)
F : a 32 bit floating point (Float_t)
D : a 64 bit floating point (Double_t)
L : a 64 bit signed integer (Long64_t)
l : a 64 bit unsigned integer (ULong64_t)


-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------


void tf::add(string file_name, string pointer_name)
---------------------------------------------------

Use this to add additional TTrees.

All arguments are the same as those of the constructor.


-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------


void tf::add(string file_name, string pointer_name, ULong64_t header_size, string data_structure)
-------------------------------------------------------------------------------------------------


All arguments are the same as those of the constructor.


-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------


void tf::sort(string pointer_name, string branch/expression, UChar_t asc/dec_opt == 0 (asc))
--------------------------------------------------------------------------------------------


Use this function to sort a TTree.


*pointer_name:*

Give the name of the pointer you use to refer to the TTree.

*branch/expression:*

Give the branch or expression of branches by which you would like the
TTree sorted.

*asc/dec_opt:*

Give either 0 to have the TTree sorted in ascending order or 1 for descending.
Ascending order is assumed.


-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------


void tf::draw(string pointer_list, string branch/leaf_list, string cuts == "", string options == "")
----------------------------------------------------------------------------------------------------


The purpose of this function is to provide a way to plot multiple graphs and histograms in tabbed sets,
with the ability to display multiple plots per canvas. The plots are given color, legends, and a text
box describing the cuts used. One may also save the plots as .gif files with the button in the upper
left corner (note that the canvas locks in place after use).

Each argument is a list of entries. Each entry is separated by a comma. The third and fourth arguments may
contain empty entries within the list (Ex: cut1,cut2,,cut3), and the total number of entries does not have
to equal that of the first two arguments (i.e. you only have to provide cuts/options for the plots that
require them.

A tilde, '~', may be added to an entry in the second, third, and fourth arguments. This means that you would
like all plots listed after this entry to have the same branches/cuts/options.


*pointer_list:*

Give a comma-separated list of the pointer names corresponding to the TTrees you would like
to draw. The number of tabs is determined by the number of groups of pointer, where a group
is either a single pointer or multiple ones surrounded by parentheses (Ex: t1 OR (t1) OR (t1,
t2, t3, etc)). The number of plots per tab is determined by the number of pointers within a
group together with the use of the 'same' option (4th argument), which is explained below. If
the 'same' option is not used, then a maximum of 4 pointers may be placed in a group.

*branch/leaf_list:*

Give a comma-separated list of branch/leaf name combinations (Ex: br1 OR br1:br2 OR
br1:br2:br3 OR br1:br2:br3:br4). As the example shows, the combinations are the same as
what you would normally provide in TTree::Draw(). Order the list such that it corresponds
to the list in the first argument. You may also use expressions involving branches/leafs
rather than a single branch/leaf (Ex: br1 * 8 + br2).

*cuts:*

Give a comma-separated list of the cuts you would like to apply. Do not use TCut objects. Again, these are
the same as what would normally be used in TTree::Draw(), using '&&' and '||' to combine selections. If no
argument is provided, the defualt is an empty string.

*options:*

Give a comma-separated list of the options you would like to apply. Many options have been tested, although
some have not. The only option that has been altered is 'same', and it may not be applied to a plot together
with any other option. Normally, the 'same' option can only be used once another plot has been made, because
it is meant allow multiple plots on the same canvas. However, for convenience, you don't have to worry about
that in tf::draw() if you don't want to, except for one circumstance where a plot is made alone in a tab
directly after a group (Ex: 1st argument --> "...(tree1,tree2,tree3),tree4..."). In this case, if you don't
follow the normal requirements of the option, then 'tree4' in the example will be drawn over the last plot
in the preceding group.

- For now, the 'same' option may not be used with a three branch/leaf entry in the second argument,
  branch/leaf_list (Ex: b1:b2:b3).


-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------


void tf::sys_info()
-------------------


This function displays the total and available RAM in GB for operating systems using a Linux kernel. It
does nothing if a different kernel is used.

It is provided because data files may be quite large, and so are there associated trees. More importantly,
you can easily create many TGraphs, and these can contain millions of data points, while histograms contain
merely hundreds. This can use a great deal of memory, and you need to keep an eye on it. Closing the plots
will free up the memory.

During a ROOT CLING session, the memory is often freed but kept by the interpreter. Thus, the memory displayed
by this function may not always show the correct amount of available memory, but you can be assured that you will not be
using more memory than what is displayed.


-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------


Example:
========

Several examples within a single interpreter session.
-----------------------------------------------------


    root [0] .L tf_class.cpp     (OR .L tf_class.cpp+)

    root [1] tf g("data/file1.trig", "a");     (OR tf* g = new tf("dtrig/file1.trig", "a");)

    root [2] g.add("data/file2.trig", "b");

    root [3] g.draw("(a,b,a,b),a,b","e,e,e,e,e:bc~","~e<6000");



    root [4] g.add("data/file3.bin", "c", 32, "l,S,D,D,i,I,b");

    root [5] g.add("data/file4.bin", "d", 32, "l,S,D,D,i,I,b");

    root [6] g.draw("(c,d,c,d,c,d,c,d),(c,d,c,d,c,d)","a,a,b,b,c,c,d,d,e:f*4,e:f*4,b:c,b:c,b:d~","f>2000 && d>=9000 && g==2~",",same,,same,,same
               ,,same,,same,,same,,same");

    root [7] g.sys_info();     (close set 1 and/or 2 if you are using too much memory)



    root [8] a->Print();

    root [9] a->Scan();

    root [10] g.sort("a","t");

    root [11] g.sort("b","t");

    root [12] g.sort("c","e",1);

    root [13] g.sort("d","e",1);

    root [14] TFile svfile("savefile.root","NEW");

    root [15] a->Write();

    root [16] b->Write();

    root [17] c->Write();

    root [18] d->Write();

    root [19] svfile.ls();

    root [20] svfile.Close();

    root [21] delete a;

    root [22] delete b;



    root [23] g.add("data/file5.root", "e");

    root [24] e_0->Print();

    root [25] e_2->Print();

    root [26] g.draw("(e_0,e_1,e_2,e_3),(e_0,e_1,e_2,e_3),(e_0,e_1,e_2,e_3)","b1,b1,b1,b1,b2,b2,b2,b2,b3:b4~","","~same");

    root [27] g.sys_info();     (close a set or two if you are using too much memory)

    root [28] g.draw("e_0,e_1,(e_3,e_2)","b1*1:b2*1:b3,b1*1:b2*1:b3,b3,b3","",",,lego~");     (see Tip 1 for the reason behind '*1')


-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------


Tips:
=====

1. Branches/leafs with ULong64_t data types must be used in an expression in TTree::Draw(), and thus tf::draw() (br*1,
   br/1, br+1-1, br%(br+1), etc. is enough). I don't know why this is necessary; as far as I know, it is an issue with
   ROOT v6.
2. Once you've loaded your trees, you can use them however you would normally do so (Draw(), Scan(), Print(), etc.).
3. The TTree::Draw() function will create a default canvas named c1 if no other canvas has been opened. Therefor,
	 if tf::draw() is used first, a new canvas must be created for TTree::Draw(); otherwise, the tf::draw() method will
	 switch back to the already created c1 canvas (assuming you haven't closed it).


-----------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------


Future Updates:
===============

- finish commenting code

- fix small memory leak with tf::draw() (not to worry though, memory will be freed upon exiting the interpreter);
  tf::sys_info() can be used to keep track of things in the mean time

- attempt to delete the dynamic arrays created by TTree::Draw(), to eliminate the extra bulk that accompanies a TTree
  after being drawn

- allow other TTree::Draw() options to be used with 'same' option in tf::draw()

- fix a bug that locks the canvas in place after the save button has been clicked in a tf::draw() set

- allow the use of the 'same' option with the TGraph2D that is produced by using three branches/leafs in TTree::Draw/tf::draw
  (Ex: b1:b2:b3).



